# RFID Mate-Kasse
## Sinn und Zweck
Die RFID Mate-Kasse (RMK) soll eine einfache Möglichkeit bieten, eine kleine Snackkasse "auf Vertrauen" zu führen.  
Um nicht jede Entnahme unmittelbar an der Kasse bezahlen zu müssen (kein passendes Kleingeld?), wird für jeden Benutzer
ein "Konto" geführt.

## Benötigte Hardware und Aufbau
Für den Betrieb wird lediglich ein Linux-PC (Raspberry Pi o.ä.), ein RFID-Lesegerät und ein Barcode-Scanner benötigt.    
Getestet wurde das [RD105-US-10H](http://www.i-keys.de/de/USB-RFID-Reader/RD105-US-10H.html) und das 
[RD-100UW](http://www.i-keys.de/de/USB-RFID-Reader/RD-100UW-Transponder-Lesegeraet.html) von [I-Keys](http://www.i-keys.de/)
mit dem [MK-1000ZB](http://www.michaelkrug.de/product_info.php/products_id/102) von [MichaelKrug.de](http://www.michaelkrug.de/).  
Grundsätzlich sollten alle RFID/Barcode-Geräte funktionieren, die sich am Rechner wie ein Eingabegerät (Keyboard) 
verhalten.

RFID-Transponder (Tags) sind in allen möglichen Formen und Farben kostengünstig zu bekommen. Z.B.: 
[nochmal Link zum I-Keys Shop](http://www.i-keys.de/de/Transponder/125-khz.html).

![RD-105](bilder/RD105-US1.jpg)
![K610](bilder/K610-X-bl-2.jpg)
![K600](bilder/K600-2.jpg)
![K691](bilder/K691-Uni-1.jpg)
![MK-1000ZB](bilder/mk1000zb.jpg)

## Wie funktioniert´s?
Jeder Benutzer bekommt ein RFID-Tag zur Identifikation ausgehändigt. Bei jeder Warenentnahme wird das Tag ans Lesegerät 
gehalten und anschließend der Artikel per Barcode eingescannt. Der Artikel und der Preis wird dann auf das Konto des 
Tags gebucht.

Der Benutzer hat die Möglichkeit, mit seinem RFID-Tag und einem speziellen Barcode seinen Kontostand abzufragen oder
sein Konto auszugleichen.

Weitere Sonderfunktionen (z. B. für den Kassenwart) sind per Kombination aus RFID (Identifikation) und Barcode (Aktion) 
in Vorbereitung.

Der Kassenwart kann darüber hinaus eine kleine Umsatzstatistik erzeugen (s.u.).

## Installation
### Vorbereitung Ubuntu/Debian Linux
Zunächst müssen ein paar erforderliche Pakete installiert werden:
<pre><code>sudo apt-get install -y redis-server python-dev python-pip git</pre></code>
Anschließend die Python Pakete:
<pre><code>sudo pip install evdev redis pyfiglet</pre></code>
### Installation per Git
<pre><code>git clone https://gitlab.com/m-busche/rfid-mate-kasse.git</pre></code>
### Update per Git
<pre><code>cd rfid-mate-kasse  
git pull</pre></code>
## Ordnerstruktur:

<pre><code>
./
./backend (Python Scripts)
./frontend (Bildschirm-Ausgabe)
</code></pre>

## Backend-Scripts

### backend/readfromdevice.py
Hinweis: readfromdevice.py benötigt root-Rechte, um die Eingabe-Events lesen zu können!

Aufruf:
<pre><code>sudo python backend/readfromdevice.py [RFID-Input-Device-ID] [EAN-Input-Device-ID]</code></pre>
Liest aus den angegebenen Input-Devices RFID-Tag und anschließend EAN-Code, hasht die Tags und schreibt sie mit 
Timestamp und Artikel-ID in die Datenbank. 
   
<pre><code>sudo python backend/readfromdevice.py -l</code></pre>Listet alle Eingabegeräte. Wird zur Ermittlung 
der Event-ID benötigt.    

Beispielhafte Ausgabe von 'backend/readfromdevice.py -l:'
<pre><code>('/dev/input/event6', 'NUMA NUMA Demo', 'usb-0000:00:14.0-1/input0')  
('/dev/input/event15', 'SMI', 'usb-0000:00:14.0-2/button')  
('/dev/input/event14', 'Logitech Unifying Device. Wireless PID:4016', 'usb-0000:00:1a.0-1.3:2')  
('/dev/input/event13', 'Logitech Unifying Device. Wireless PID:401b', 'usb-0000:00:1a.0-1.3:1')  
('/dev/input/event12', 'HDA Intel PCH HDMI/DP,pcm=3', 'ALSA')  
('/dev/input/event11', 'HDA Intel PCH Headphone', 'ALSA')  
('/dev/input/event10', 'HDA Intel PCH Mic', 'ALSA')  
('/dev/input/event9', 'NEKO Microe eRFID USB Rea', 'usb-0000:00:14.0-3/input1')  
('/dev/input/event8', 'NEKO Microe eRFID USB Rea', 'usb-0000:00:14.0-3/input0')  
('/dev/input/event7', 'FSPPS/2 Sentelic FingerSensingPad', 'isa0060/serio1/input0')  
('/dev/input/event6', 'NUMA NUMA Demo', 'usb-0000:00:14.0-1/input0')  
('/dev/input/event5', 'Video Bus', 'LNXVIDEO/video/input0')  
('/dev/input/event4', 'AT Translated Set 2 keyboard', 'isa0060/serio0/input0')  
('/dev/input/event3', 'Power Button', 'LNXPWRBN/button/input0')  
('/dev/input/event2', 'Lid Switch', 'PNP0C0D/button/input0')  
('/dev/input/event1', 'Power Button', 'PNP0C0C/button/input0')  
('/dev/input/event0', 'Sleep Button', 'PNP0C0E/button/input0')  
</pre></code>
Das RFID-Gerät lauscht hier auf event8, der Barcodescanner auf event6. Der Aufruf würde dann 
<pre><code>sudo python backend/readfromdevices.py 3 8</pre></code> lauten. 
  

### backend/createdb.py

Erstellt eine neue (leere!) Sqlite-Datenbank und die benötigten Tabellen. Falls eine alte Datenbankdatei gefunden wird, 
wird diese vorher gesichert.  

Erfordert keine weiteren Parameter. Die Datenbank wird im aktuellen Verzeichnis erstellt.

### backend/matekasse.py

Aufruf:
<pre><code>matekasse.py [RFID] [Action]</code></pre>

\[RFID\]: RFID-Tag oder * für Kassensturz  
\[Action\]: \[konto|pay|kasse\] (case sensitive!)

*konto*: Zeigt den Kontostand an (Anzahl unbezahlter Getränke der jeweiligen RFID)  
*pay*: Setzt alle unbezahlten Einträge der RFID auf bezahlt  
*kasse*: Gibt eine Statistik der Umsätze aus  

Hint: *matekasse.py* stürzt ab, wenn nicht beide Parameter übergeben werden!

#### Beispiele Ausgabe

<pre><code>/backend/matekasse.py * kasse
Datensätze gesamt: 8
Bezahlte Getränke: 1
Summe bezahlte Getränke: 1.50 EUR
Unbezahlte Getränke: 7 
Summe unbezahlte Getränke: 10.50 EUR</code></pre>

<pre><code>backend/matekasse.py 5900078f5f konto
Getränke: 5
Kontostand: 7.50 EUR</code></pre>

<pre><code>backend/matekasse.py 5900078f5f pay
5 Getränke bezahlt.</code></pre>


### backend/dbmodule.py
Nur ein paar Datenbankfunktionen.

## Frontend-Scripts
### frontend/display.py
Bildschirmausgabe der Buchungsvorgänge

## Hinweise
Die Datenbank *rfid-mate-kasse.sqlite* liegt in ./
Die Scripte müssen aus diesem Ordner heraus gestartet werden.

## Datenbankstruktur
![Matekasse Buchungen](bilder/matekasse-buchungstabelle.png "Buchungen")  
Buchungen  
![Matekasse Artikel](bilder/matekasse-artikeltabelle.png "Artikel")  
Artikel  


## Bildmaterial
Die Abbildungen der Transponder und Lesegeräte wurden freundlicherweise von

I-KEYS · RFID-Technik  
Buero/Office: Sieglindestrasse 9  
12159 Berlin  
Germany  
[http://www.i-keys.de/](http://www.i-keys.de/)

zur Verfügung gestellt.

Die Abbildung des Barcode-Scanners wurde freundlicherweise von

Michael Krug  
Ludwigstraße 31a  
83278 Traunstein  
Deutschland - Germany  
[http://www.michaelkrug.de/](http://www.michaelkrug.de/)  

zur Verfügung gestellt.