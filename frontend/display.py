#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'markus'
# https://stackoverflow.com/questions/27745842/redis-pubsub-and-message-queueing
# https://github.com/pwaller/pyfiglet
import redis
import time
import os
import ConfigParser
from pyfiglet import Figlet


def configsectionmap(myconfig, section):
    """Read value from INI"""
    dict1 = {}
    options = myconfig.options(section)
    for option in options:
        try:
            dict1[option] = myconfig.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None

    return dict1


Config = ConfigParser.ConfigParser()
Config.read("rfid-mate-kasse.ini")

screenwidth = configsectionmap(Config, "display")['width']
font = configsectionmap(Config, "display")['font']

f = Figlet(font=font, width=screenwidth)

r = redis.StrictRedis(host='localhost', port=6379, db=0)
p = r.pubsub()
p.subscribe('status')
p.subscribe('action')
p.subscribe('text')

os.system('clear')

while True:
    message = p.get_message()
    if message:
        if message['channel'] == "action":
            if message['data'] == "clear":
                os.system('clear')
        else:
            # print message['data']
            if message['channel'] == 'status':
                print f.renderText(str(message['data']))
            else:
                print (str(message['data']))
    time.sleep(0.5)