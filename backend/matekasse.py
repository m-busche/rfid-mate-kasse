#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'markus'
import sys
import hashlib
import dbmodule
from datetime import datetime, date


def getchipid():
    "commandline parsen"
    try:
        chipid = str(sys.argv[1]).strip().upper()
        chipidhashed = hashlib.md5(chipid).hexdigest()
        action = sys.argv[2]
        # sys.stdout.write("chipid: '" + chipid + "'\n")
        # sys.stdout.write("chipidhashed: " + chipidhashed + "\n")
        return (chipidhashed, action)
        # return (chipid, action)
    except Error, e:
        sys.stdout.write("Aufruf: matekasse.py RFID Action")
        sys.stdout.write("Action: [konto|pay|kasse]")
        exit(2)


chip = getchipid()[0]
action = getchipid()[1]

if chip == "*":
    stats = dbmodule.kassensturz()
    sys.stdout.write("Datensätze gesamt: " + str(stats[0] + stats[2]) + "\n")
    sys.stdout.write("Bezahlte Getränke: " + str(stats[2]) + "\n")
    sys.stdout.write("Summe bezahlte Getränke: " + str(stats[3]) + " EUR\n")
    sys.stdout.write("Unbezahlte Getränke: " + str(stats[0]) + " \n")
    sys.stdout.write("Summe unbezahlte Getränke: " + str(stats[1]) + " EUR\n")
    exit(0)

if action == "konto":
    countkonto = dbmodule.getbalance(chip)
    count = str(countkonto[0])
    konto = str(countkonto[1])
    sys.stdout.write("Konto: " + chip + "\n")
    if konto == "-1":
        sys.stdout.write("Konto nicht gefunden.")
    else:
        sys.stdout.write("Getränke: " + count + "\n")
        sys.stdout.write("Kontostand: " + konto + " EUR \n")
elif action == "pay":
    bezahlt = dbmodule.pay(chip, 1)
    if bezahlt > 1:
        sys.stdout.write(str(bezahlt) + " Getränke bezahlt." )
    else:
        sys.stdout.write(str(bezahlt) + " Getränk bezahlt." )
else:
    sys.stdout.write("Nothing to do.")
    exit(0)
