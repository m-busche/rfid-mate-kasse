#!/usr/bin/python
# -*- coding: utf-8 -*-

# https://stackoverflow.com/questions/19732978/how-can-i-get-a-string-from-hid-device-in-python-with-evdev
import sys
import hashlib
import os
import redis
import time
import dbmodule
import ConfigParser
from evdev import InputDevice, ecodes, categorize, list_devices  # import * is evil :)


def configsectionmap(myconfig, section):
    """Read value from INI"""
    dict1 = {}
    options = myconfig.options(section)
    for option in options:
        try:
            dict1[option] = myconfig.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None

    return dict1


def getdatafromdevice(myinputid, mymuteid):
    """Read events from device inputid and get data as string, mutes device muteid, returns: String"""
    iputdev = InputDevice('/dev/input/event' + str(myinputid))
    mutedev = InputDevice('/dev/input/event' + str(mymuteid))
    iputdev.grab()
    mutedev.grab()
    ausgabe = ""
    scancodes = {
        # Scancode: ASCIICode
        0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
        10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'BKSP', 15: u'TAB', 16: u'Q', 17: u'W', 18: u'E', 19: u'R',
        20: u'T', 21: u'Y', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 26: u'[', 27: u']', 28: u'CRLF', 29: u'LCTRL',
        30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 39: u';',
        40: u'"', 41: u'`', 42: u'LSHFT', 43: u'\\', 44: u'Z', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N',
        50: u'M', 51: u',', 52: u'.', 53: u'/', 54: u'RSHFT', 56: u'LALT', 100: u'RALT', 96: u'ENTER'
    }

    for event in iputdev.read_loop():
        if event.type == ecodes.EV_KEY:
            data = categorize(event)  # Save the event temporarily to introspect it
            if data.keystate == 1:  # Down events only
                # Abbruch bei ENTER (RFID) oder CRLF (EAN)
                if data.scancode == 96 or data.scancode == 28:
                    break
                key_lookup = scancodes.get(data.scancode) or u'UNKNOWN:{}'.format(data.scancode)  # Lookup or return UNKNOWN:XX
                # print u'You Pressed the {} key!'.format(key_lookup)  # Print it all out!
                ausgabe += str(key_lookup)
    iputdev.ungrab()
    mutedev.ungrab()
    return ausgabe


def getinpudevices():
    """commandline parsen"""
    try:
        if sys.argv[1] == "-l":
            return "list"
        rfiddevice = sys.argv[1]
        eandevice = sys.argv[2]
        return rfiddevice, eandevice
        # sys.stdout.write("RFID: " + chipid)
    except:
        sys.stdout.write("Aufruf: readfromdevice.py [RFID Input-Device-ID] [EAN Input-Device-ID]\n"
                         "Oder readfromdevice.py -l zum Auflisten der Eingabegeräte.")
        exit(2)


def listinputdevices():
    """List valid input devices"""
    devices = [InputDevice(fn) for fn in list_devices()]
    print('Devices:\n')
    for dev in devices:
        print(dev.fn, dev.name, dev.phys)


def kontostand(myuserid):
    """Get and print Kontostand"""
    countkonto = dbmodule.getbalance(myuserid)
    queue.publish("status", "Dein Kontostand: " + str(countkonto) + " EUR")


def specialaction(eancode, rfidtag, uid):
    """Führt spezielle Vorgänge für Kombinationen aus eancode/rfidtag durch, Returns: False für nicht zutreffend, \
    sonst True"""
    global hackattempts
    global rfid_cashier, rfid_guest, rfid_admin
    global ean_pay1, ean_pay5, ean_pay10, ean_cleardb, ean_kasse, ean_gratis, ean_reboot
    uid = str(uid)
    print('ean: ', eancode)
    print('ean_pay1', ean_pay1)
    # Datenbank leeren
    if eancode == ean_cleardb:
        if rfidtag == rfid_admin:
            dbmodule.emptybookings()
            queue.publish("text", "Buchungstabelle wurde geleert.")
            hackattempts = 0
            return True
        else:
            queue.publish("text", "Sorry. This tag is not authorized for this function.\nhackattempts += 1\n")
            hackattempts += 1
            return False
    if eancode == ean_kasse:
        if rfidtag == rfid_cashier:
            stats = dbmodule.kassensturz()
            # return unpaidcount, unpaidsum, paidcount, paidsum
            queue.publish("status", 'clear')
            queue.publish("text", 'Unbezahlte Artikel: ' + str(stats[0]))
            queue.publish("text", 'Aussenstände: ' + str(stats[1]))
            queue.publish("text", 'Bezahlte Artikel: ' + str(stats[2]))
            queue.publish("text", 'Geldeingang: ' + str(stats[3]))
            time.sleep(15)
            hackattempts = 0
            return True
        else:
            queue.publish("text", "Sorry. This tag is not authorized for this function.\nhackattempts += 1\n")
            hackattempts += 1
            return False
    if eancode == ean_gratis:
        return True
    if eancode == ean_reboot:
        if rfidtag == rfid_admin:
            os.system('reboot')
            # print ('reboot now.')
            hackattempts = 0
            return True
        else:
            queue.publish("text", "Sorry. This tag is not authorized for this function.\nhackattempts += 1\n")
            hackattempts += 1
            return False
    if eancode == ean_pay1:
        print('uid: ', uid)
        dbmodule.pay(uid, 1)
        kontostand(rfidtag)
    if eancode == ean_pay5:
        print('uid: ', uid)
        dbmodule.pay(uid, 5)
        kontostand(rfidtag)
    if eancode == ean_pay10:
        print('uid: ', uid)
        dbmodule.pay(uid, 10)
        kontostand(rfidtag)
    return False


if os.getuid() != 0:
    sys.stdout.write("This program was not run as sudo or otherwise elevated. This it will probably not work!")
    exit(1)


config = ConfigParser.SafeConfigParser()
config.read("rfid-mate-kasse.ini")
# rfiddevice = configsectionmap("inputdevices")['rfid']
# eandevice = configsectionmap("inputdevices")['ean']
ean_pay1 = configsectionmap(config, "eanactions")['pay1'].upper()
ean_pay5 = configsectionmap(config, "eanactions")['pay5'].upper()
ean_pay10 = configsectionmap(config, "eanactions")['pay10'].upper()
ean_cleardb = configsectionmap(config, "eanactions")['cleardb'].upper()
ean_cleardb = str(ean_cleardb).upper()
ean_kasse = configsectionmap(config, "eanactions")['kasse'].upper()
ean_gratis = configsectionmap(config, "eanactions")['gratis'].upper()
ean_reboot = configsectionmap(config, "eanactions")['reboot'].upper()
rfid_admin = str(configsectionmap(config, "rfid-auth")['admin']).upper()
rfid_admin = hashlib.md5(rfid_admin).hexdigest()
rfid_guest = str(configsectionmap(config, "rfid-auth")['guest']).upper()
rfid_guest = hashlib.md5(rfid_guest).hexdigest()
rfid_cashier = str(configsectionmap(config, "rfid-auth")['cashier']).upper()
rfid_cashier = hashlib.md5(rfid_cashier).hexdigest()
hackattempts = 0

if getinpudevices() == "list":
    listinputdevices()
    exit(0)
else:
    queue = redis.StrictRedis(host='localhost', port=6379, db=0)
    channel = queue.pubsub()
    idprice = []
    inputid = getinpudevices()
    queue.publish("action", "clear")
    while 1:
        queue.publish("status", "RFID-Matekasse\n\n")
        queue.publish("status", "Waiting for RFID-Tag...")
        rfid = getdatafromdevice(inputid[0], inputid[1]).upper()
        rfidhashed = hashlib.md5(rfid).hexdigest()
        userid = dbmodule.getuser(rfidhashed)

        if userid == 0:
            # Benutzer noch unbekannt
            queue.publish("action", "clear")
            queue.publish("status", "RFID-Matekasse\n\n")
            queue.publish("text", "Moin und Willkommen! Wir beiden kennen uns noch nicht.\n\n")
            time.sleep(2)
            queue.publish("text", "Ich bin die RFID-Mate-Kasse. Und du bist der/die mit dem Tag\n\n")
            queue.publish("status", rfid + "\n")
            queue.publish("text", "Das Tag in lesbar: " + rfid + "\n")
            queue.publish("text", "Fingerprint: " + rfidhashed + "\n")
            queue.publish("text", "Das Tag solltest du dir notieren, falls du es mal verlierst.\n")
            queue.publish("text", "Ich speichere lediglich den Fingerprint.\n")
            queue.publish("text", "Halte das Tag noch einmal an das Gerät, dann geht's weiter.")
            getdatafromdevice(inputid[0], inputid[1]).upper()
            queue.publish("action", "clear")
            queue.publish("status", "Viel Spass im Space!")
            dbmodule.adduser(rfidhashed, "")
        else:
            # Benutzer bekannt
            queue.publish("text", "Dein Tag (hashed): " + rfidhashed)
            kontostand(userid)
            queue.publish("status", "Waiting for EAN...")
            ean = getdatafromdevice(inputid[1], inputid[0])
            idprice = dbmodule.getproduct(ean)
            # Spezialaktion?
            if specialaction(ean, rfidhashed, userid):
                print('Special action done..')
            else:
                articleid = idprice[0]
                article = idprice[1]
                articleprice = idprice[2]

            if not articleid == 0:
                # Artikel vorhanden, erfolgreiche Buchung
                dbmodule.writebooking(userid, articleid, articleprice)
                dbmodule.setbalance(userid, articleprice * -1)
                queue.publish("status", "Artikel: " + article)
                queue.publish("status", "Preis: " + str(articleprice) + " EUR")
                kontostand(userid)
            else:
                # Unbekannter Artikel
                queue.publish("status", "Unbekannter Artikel!")

        time.sleep(5)
        queue.publish("action", "clear")