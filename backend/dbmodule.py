#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'markus'
import sys
from datetime import datetime
from sqlite3 import connect, PARSE_DECLTYPES, Error


def getbalance(userid):
    """holt Kontostand für User-ID [userid], returns: (Kontostand)"""
    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("select balance from users where id like " + str(userid))
            rows = cur.fetchone()
            balance = rows[0]
    except Error:
        # Keine Treffer
        balance = -1
    finally:
        if con:
            con.close()
    return balance


def setbalance(userid, value):
    """ändert den Kontostand für User-ID [userid] um value, returns: (neuer Kontostand)"""
    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("select balance from users where id like (?)", str(userid))
            rows = cur.fetchall()
            balance = rows[0]
            newbalance = balance[0] + value
            cur.execute("update users set balance=? where id like ?",
                        (newbalance, userid))

    except Error:
        # Keine Treffer
        newbalance = -1
    finally:
        if con:
            con.close()
    return newbalance


def getproduct(ean):
    """Sucht in der Artikeltabelle nach ean. Returns: (article-id, article, price)"""
    con = None
    thisid = 0
    article = ""
    price = 0

    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("select id, article, price from articles where ean like '" + str(ean) + "';")
            rows = cur.fetchall()
            for row in rows:
                thisid = row[0]
                article = row[1]
                price = row[2]
    except Error, e:
        thisid = 0
        article = ""
        price = 0
    finally:
        if con:
            con.close()
    return thisid, article, price


def createdb():
    """create new database and table"""
    con = None

    try:
        con = con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("drop table if exists 'bookings';")
            cur.execute("CREATE TABLE bookings (id integer primary key autoincrement, userid integer, "
                        "[timestamp] timestamp, 'price' real, 'articleid' integer, 'paid' integer, "
                        "[paidtimestamp] timestamp)")
            cur.execute("drop table if exists 'articles';")
            cur.execute("CREATE TABLE 'articles' (id integer primary key autoincrement, 'ean' text, 'article' text, "
                        "'price' real);")
            cur.execute("drop table if exists 'users';")
            cur.execute("CREATE TABLE 'users' (id integer primary key autoincrement, 'rfid' text, 'username' text, "
                        "'balance' real);")
    except Error, e:
        sys.stdout.write('Error: ' + e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()
    return


def emptybookings():
    """empty table 'bookings'"""
    con = None

    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("delete from 'bookings';")
            cur.execute("vacuum;")
    except Error, e:
        sys.stdout.write('Error: ' + e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()
    return


def writebooking(userid, articleid, articleprice):
    """Schreibt User-ID, Timestamp, article-ID, article-Preis, 0 (nicht bezahlt) in bookings"""
    con = None

    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("insert into bookings(userid, timestamp, price ,articleid, paid) values (?, ?, ?, ?, ?)",
                        (userid, datetime.now(), articleprice, articleid, 0))

    except Error, e:
        sys.stdout.write('Error: ' + e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()
    return


def kassensturz():
    """holt Statistik, returns: (Anzahl unbezahlter Getränke, Summe unbezahlter Getränke, \
    Anzahl bezahlter Getränke, Summe bezahlter Getränke)"""

    con = None
    unpaidsum = 0
    paidsum = 0

    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            # unbezahlte Getränkeentnahmen
            cur.execute("select * from bookings where paid = 0")
            rows = cur.fetchall()
            for row in rows:
                unpaidsum += row[3]
            unpaidcount=rows.__len__()

            cur.execute("select * from bookings where paid = 1")
            rows = cur.fetchall()
            for row in rows:
                paidsum += row[3]
            paidcount = rows.__len__()

    except Error, e:
        sys.stdout.write('Error: ' + e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()

    return unpaidcount, unpaidsum, paidcount, paidsum


def pay(thisuserid, amount):
    """Setzt 1 oder alle Einträge auf bezahlt"""
    # TODO Einzeln zahlen via amount
    con = None

    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            # Hole Kontostand
            cur.execute("select balance from users where id like (?)", str(thisuserid))
            balance = cur.fetchone()[0]
            # import pdb; pdb.set_trace()
            newbalance = balance + amount
            # Setze auf bezahlt
            cur.execute("update users set balance=? where id like ?",
                        (newbalance, thisuserid))
    except Error, e:
        sys.stdout.write('Error: ' + e.args[0])
        sys.exit(1)
    finally:
        if con:
            con.close()
    return True


def getuser(rfid):
    """Sucht in Tabelle users nach RFID. Returns: ID oder 0"""
    con = None
    thisid = 0
    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("select id rfid from users where rfid like '" + str(rfid) + "';")
            rows = cur.fetchall()
            for row in rows:
                thisid = row[0]
    except Error, e:
        thisid = 0
    finally:
        if con:
            con.close()
    return thisid


def adduser(rfid, username):
    """Schreibt neuen Benutzer in die Tabelle 'users'. Returns: 1 (Fehler) oder 0 (Ok)"""
    con = None
    result = 0
    try:
        con = connect('rfid-mate-kasse.sqlite', detect_types=PARSE_DECLTYPES)
        with con:
            cur = con.cursor()
            cur.execute("insert into users (rfid, username, balance) values (?, ?, ?);",
                        (rfid, username, 0))
    except Error:
        result = 1
    finally:
        if con:
            con.close()
    return result
