#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'elpatron'

import dbmodule
import sys
import os.path
import shutil
from datetime import datetime, date



# Sicher ist sicher
if os.path.isfile('rfid-mate-kasse.sqlite'):
    backupfilename = 'rfid-mate-kasse.' + str(datetime.now()) + ".sqlite"
    shutil.copyfile('rfid-mate-kasse.sqlite', backupfilename)

# Neue Datenbank
dbmodule.createdb()
sys.stdout.write("Neue Datenbank angelegt.\n")
